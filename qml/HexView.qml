/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * HexExplorer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import QtQuick.Controls 2.2 as QC
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3 as LTP //for Dialogs

Page {
    id: mainPage
    anchors.fill: parent

    property string hexString: "#" + redHexString + greenHexString + blueHexString
    property int redHexValue: sliderRed.value
    property int greenHexValue: sliderGreen.value
    property int blueHexValue: sliderBlue.value
    property string redHexString: (redHexValue < 16) ? "0" + redHexValue.toString(16) : redHexValue.toString(16)
    property string greenHexString: (greenHexValue < 16) ? "0" + greenHexValue.toString(16) : greenHexValue.toString(16)
    property string blueHexString: (blueHexValue < 16) ? "0" + blueHexValue.toString(16) : blueHexValue.toString(16)

    header: PageHeader {
        id: header
        title: i18n.tr('HexExplorer')

        StyleHints {
            foregroundColor: appSettings.persistentInkColor
            backgroundColor: appSettings.persistentBackgroundColor
            dividerColor: Qt.darker(backgroundColour)
        }

        trailingActionBar {
                actions: [
                    Action {
                        id: aboutAction
                        iconName: "info"
                        text: i18n.tr("About")
                        onTriggered: mainPageStack.push(Qt.resolvedUrl("About.qml"))
                    },
                    Action {
                        id: settingsAction
                        iconName: "settings"
                        text: i18n.tr("Settings")
                        onTriggered: mainPageStack.push(Qt.resolvedUrl("Settings.qml"))
                    },
                    Action {
                        id: manualAction
                        iconName: "edit"  //preferences-color-symbolic  //keyboard
                        text: i18n.tr("Manual input")
                        onTriggered: PopupUtils.open(manualInputDialog)
                    }
                ]
            }
    }

    Component.onCompleted: {
        sliderRed.value = appSettings.persistentRedHexValue
        sliderGreen.value = appSettings.persistentGreenHexValue
        sliderBlue.value = appSettings.persistentBlueHexValue
    }

    Rectangle {
        id: hexSection
        width: parent.width
        height: parent.height/11
        anchors.top: header.bottom
        color: root.backgroundColor

        TextInput {
            id: sliderValue
            anchors.centerIn: parent
            color: inkColour
            text: hexString
            font.pixelSize: units.gu(4)
            readOnly: true
            selectByMouse: true
            mouseSelectionMode: TextInput.SelectCharacters
            //selectByKeyboard: true
        } //sliderValue
        Rectangle {
            anchors {
                left: sliderValue.right
                leftMargin: units.gu(2)
                verticalCenter: sliderValue.verticalCenter
            }
            height: sliderValue.height
            width: height
            color: hexCopy.pressed ? inkColour : "transparent"
            MouseArea {
                id: hexCopy
                anchors.fill: parent
                onClicked: Clipboard.push(hexString)
            }
            Icon {
                height: parent.height * 0.75
                anchors.centerIn: parent
                color: inkColour
                width: height
                name: "edit-copy"
            }
        }
    } //hexSection

    Rectangle {
        id: colorSection
        width: parent.width
        height: parent.height/11 * 5
        anchors.top: hexSection.bottom
        color: "transparent"

        Rectangle {
            id: colorRectangle
            width: parent.width/6 * 5
            height: parent.height/10 * 9
            anchors.centerIn: parent
            color: hexString
            border.color: Qt.darker(hexString)
            border.width: 2
            radius: 10

        } //colorRectangle

    } //colorSection

    Rectangle {
        id: sliderSection
        width: parent.width
        height: parent.height - (header.height + hexSection.height + colorSection.height)
        anchors.top: colorSection.bottom
        color: "transparent"

        Rectangle {
            id: sliderWrapper
            width: parent.width/6 * 5
            height: parent.height/5 * 4
            color: "transparent"
            anchors.centerIn: parent

            Rectangle {
                id: redSliderSection
                width: parent.width
                height: parent.height/3
                anchors.top: parent.top
                color: parent.color

                Rectangle {
                    id: redSliderRect
                    color: "#" + redHexString + "0000"
                    width: parent.width/6
                    height: parent.height/2
                    anchors.centerIn: parent
                    radius: 15
                    border.color: Qt.darker(redSliderRect.color)
                    border.width: 2
                    Label {
                        anchors.centerIn: parent
                        font.bold: true
                        text: i18n.ctr("first letter of color red as capital letter","R")
                    }
                } //redSliderRect

                QC.Slider {
                    id: sliderRed
                    anchors.centerIn: parent
                    width: parent.width
                    height: parent.height
                    handle.height: appSettings.standardFontSize
                    handle.width: handle.height
                    from: 0x00
                    value: 0x00
                    to: 0xff
                    stepSize: 0x01
                    snapMode: QC.Slider.SnapAlways
                    onMoved: appSettings.persistentRedHexValue = value
                } //sliderRed
            } //redSliderSection

            Rectangle {
                id: greenSliderSection
                width: parent.width
                height: parent.height/3
                anchors.top: redSliderSection.bottom
                color: "transparent"

                Rectangle {
                    id: greenSliderRect
                    color: "#00" + greenHexString + "00"
                    width: parent.width/6
                    height: parent.height/2
                    anchors.centerIn: parent
                    radius: 15
                    border.color: Qt.darker(greenSliderRect.color)
                    border.width: 2
                    Label {
                        anchors.centerIn: parent
                        font.bold: true
                        text: i18n.ctr("first letter of color green as capital letter","G")
                    }
                } //greenSliderRect

                QC.Slider {
                    id: sliderGreen
                    anchors.centerIn: parent
                    width: parent.width
                    height: parent.height
                    handle.height: appSettings.standardFontSize
                    handle.width: handle.height
                    from: 0x00
                    value: 0x00
                    to: 0xff
                    stepSize: 0x01
                    snapMode: QC.Slider.SnapAlways
                    onMoved: appSettings.persistentGreenHexValue = value
                } //sliderGreen
            } //greenSliderSection

            Rectangle {
                id: blueSliderSection
                width: parent.width
                height: parent.height/3
                anchors.top: greenSliderSection.bottom
                color: "transparent"

                Rectangle {
                    id: blueSliderRect
                    color: "#0000" + blueHexString
                    width: parent.width/6
                    height: parent.height/2
                    anchors.centerIn: parent
                    radius: 15
                    border.color: Qt.darker(blueSliderRect.color)
                    border.width: 2
                    Label {
                        anchors.centerIn: parent
                        font.bold: true
                        text: i18n.ctr("first letter of color blue as capital letter","B")
                    }
                } //blueSliderRect

                QC.Slider {
                    id: sliderBlue
                    anchors.centerIn: parent
                    width: parent.width
                    height: parent.height
                    handle.height: appSettings.standardFontSize
                    handle.width: handle.height
                    from: 0x00
                    value: 0x00
                    to: 0xff
                    stepSize: 0x01
                    snapMode: QC.Slider.SnapAlways
                    onMoved: appSettings.persistentBlueHexValue = value
                } //sliderBlue
            } //blueSliderSection
        } //sliderWrapper
    } //sliderSection

    Component {
        id: manualInputDialog

        LTP.Dialog {
            id: inputDialog
            title: i18n.tr("Manual hex input")
            contentHeight: units.gu(30)
            contentWidth: units.gu(35)

            Column {
                spacing: units.gu(2)
                Row {
                    anchors.horizontalCenter: parent.horizontalCenter
                    Label {
                        id: enterHexLabel
                        text: i18n.tr("Enter Hex:  #")
                        anchors.verticalCenter: parent.verticalCenter
                        font.pixelSize: appSettings.standardFontSize
                    } //enterHexLabel
                    TextField {
                        id: hexInput
                        width: units.gu(12)
                        anchors.verticalCenter: parent.verticalCenter
                        maximumLength: 6
                        inputMask: "HHHHHH"
                        selectByMouse: true
                        inputMethodHints: Qt.ImhPreferNumbers
                    } //hexInput
                }

                Button {
                    id: changeHexButton
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: i18n.tr("Apply")
                    color: theme.palette.normal.positive
                    font.pixelSize: appSettings.standardFontSize

                    onClicked: {
                        if (hexInput.text.length == 6) {
                            sliderRed.value = parseInt(hexInput.text[0] + hexInput.text[1] , 16);
                            sliderGreen.value = parseInt(hexInput.text[2] + hexInput.text[3], 16);
                            sliderBlue.value = parseInt(hexInput.text[4] + hexInput.text[5], 16);
                            PopupUtils.close(inputDialog)
                        }
                        else if (hexInput.text.length == 0) {
                            hexInput.cursorPosition = 0;
                            hexInput.focus = true;
                        }
                        else {
                            hexInput.cursorPosition = hexInput.text.length;
                            hexInput.focus = true;
                        }
                    } //onClicked
                } //changeHexButton

                Button {
                    id: cancelButton
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: i18n.tr("Cancel")
                    font.pixelSize: appSettings.standardFontSize

                    onClicked: PopupUtils.close(inputDialog)
                } //cancelButton
            }
        }
    }
}
