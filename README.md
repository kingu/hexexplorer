# HexExplorer revived!

## Credits
This is a fork of the [orignial app](https://github.com/perryhelion/Hex-Explorer) made by [pHeLiOn](https://github.com/perryhelion). Since that seems unmaintained I copied the code and improved it to keep the app alive.

## App description

Move the Sliders, see the colour, read the Hexadecimal representation. Try the colors with the apps background and text color settings.

This is a fairly simple program that might prove useful to anyone unfamiliar with the 16 million or so colours available to them when programming in any computer language that uses the hexadecimal numbers to represent the three colour components of the RGB system.

Each colour component has 256 different values (from 0 to 255) and can be 'mixed' together to make pretty much any colour in any shade or hue that you could want.

The original developer made the app as a learning exercise for him to see how straightforward or not it was to program in QML. He wanted to use the Ubuntu SDK mainly using mainly Qt Quick Controls instead of 'Ubuntu Components 1.3' (although still present to make use of 'i18n.tr()' functions). This should theoretically make the app more easily portable to other platforms.

## Re-release purposes

The original creator does not maintain the app anymore. The main purpose of this re-release is to keep the app maintained. One fundamental change is the switch to a pure qml type app dropping the unneeded c++ parts. This does make the app architecture independed. It is also implementing back the Ubuntu Touch look and feel with header, as well as adding some features and fixing some bugs. The code is migrated from github to gitlab.

## Translating

HexExplorer app can be translated on [Hosted Weblate](https://hosted.weblate.org/projects/ubports/hexexplorer/). The localization platform of this project is sponsored by Hosted Weblate via their free hosting plan for Libre and Open Source Projects.

We welcome translators from all different languages. Thank you for your contribution!
You can easily contribute to the localization of this project (i.e. the translation into your language) by visiting (and signing up with) the Hosted Weblate service as linked above and start translating by using the webinterface. To add a new language, log into weblate, goto tools --> start new translation.

## Licence

Copyright (C) 2020  P Helion

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3, as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
